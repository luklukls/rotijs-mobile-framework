roti.newView('mhs',{
        id:'mhs',
        items:[
                {
                        type:'tile',                        
                        id:'tile',
                        items:[
                                {
                                        id:'homebtn',
                                        icon:true,
                                        content:'<i class="icon-arrow-up-left"></i>',                                       
                                        bgcolor:'red',
                                        info:{
                                            name:'Haaman Depan'
                                        },
                                        event:{
                                            ontap:function(){                                                
                                                roti.menu='home';
                                                roti.view('home');



                                            }
                                        }
                                }, 
                                {
                                        id:'profile',
                                        mode:'double',
                                        content:'<img src="images/pro.jpg" class="place-right" width="60px">'+
                                        '<h6 style="margin-bottom: 5px;" id="profile_title"></h6>'+
                                        '<p id="profile_info"></p>',                                        
                                        bgcolor:'greenDark',
                                        info:{
                                                
                                        }

                                },                 
                                {
                                        id:'inbox',
                                        mode:'double',
                                        content:'<h5 id="inbox_title"></h5><p id="inbox_info"></p>',                                        
                                        bgcolor:'blue',
                                        info:{

                                            icon:'Mail128.png',
                                            badge:{
                                                number:1,
                                                bgcolor:'red'
                                            }
                                        },

                                        event:{
                                            ontap:function(){
                                                
                                                roti.views['contentView'].items[0].title="Inbox";          
                                                roti.view('contentView',function(){
                                                var inbox=roti.create({
                                                        type:'inbox',
                                                        date:'01/01/2013',
                                                        title:'Panduang menggunakan Siadin Mobile',
                                                        text:'testing inbox testing',
                                                });
                                                roti.add(inbox,'htmlContent');                                                
                                                siadinAPI.call('inbox',{},function(response){
                                                    if(response.status=="OK")
                                                    {
                                                        for(var i in response.data)
                                                        {
                                                            var inbox=roti.create({
                                                                type:'inbox',
                                                                date:'01/01/2013',
                                                                title:response.data[i].subject,
                                                                text:response.data[i].text,
                                                            });
                                                            roti.add(inbox,'htmlContent');
                                                        }
                                                        
                                                    }
                                                });
                                            
                                            });
                                            }
                                        }

                                },                                                 
                                {
                                        id:'absensi',
                                        icon:true,
                                        content:'<i class="icon-flag-2"></i>',                                       
                                        bgcolor:'green',
                                        info:{
                                                name:'Absensi',                                                
                                        },    
                                        event:{
                                            ontap:function(){
                                                roti.views['contentView'].items[0].title="Absensi";          
                                                roti.view('contentView',function(){
                                                siadinAPI.call('absensi_mhs',{},function(response){
                                                    if(response.status=="OK")
                                                    {
                                                        
                                                        roti.add(roti.create({
                                                            type:'grid',
                                                            json:response.data
                                                        }),'htmlContent');

                                                    }
                                                });
                                            })                                                
                                            }
                                        }                                    

                                },                       

                                {
                                        id:'KRS',
                                        mode:'icon',
                                        content:'<i class="icon-target"></i>',                                       
                                        bgcolor:'yellow',
                                        info:{
                                                name:'KRS',                                                
                                        },                                        
                                        event:{
                                            ontap:function(){
                                                roti.views['contentView'].items[0].title="KRS";          
                                                roti.view('contentView',function(){
                                                siadinAPI.call('krs_mhs',{},function(response){
                                                    if(response.status=="OK")
                                                    {
                                                        roti.add(roti.create({
                                                            type:'grid',
                                                            json:response.data
                                                        }),'htmlContent');

                                                    }
                                                });
                                                });
                                            }
                                        }                                    
                                },                       
                                {
                                        id:'nilai',
                                        icon:true,
                                        content:'<i class="icon-stats"></i>',                                       
                                        bgcolor:'blueDark',
                                        info:{
                                                name:'Nilai Transkrip',                                                
                                        },               
                                        event:{
                                            ontap:function(){
                                                roti.views['contentView'].items[0].title="Transkip Mahasiswa";          
                                                roti.view('contentView',function(){
                                                siadinAPI.call('transkip_mhs',{},function(response){
                                                    if(response.status=="OK")
                                                    {
                                                        roti.add(roti.create({
                                                            type:'grid',
                                                            json:response.data
                                                        }),'htmlContent');

                                                    }
                                                });
                                                });
                                            }
                                        }                                                                                                     

                                },                                                        
                                {
                                        id:'kartuujian',
                                        mode:'icon',
                                        content:'<i class="icon-libreoffice"></i>',                                       
                                        bgcolor:'pinkDark',
                                        info:{
                                                name:'Kartu Ujian',                                                
                                        },                                        
                                        event:{
                                            ontap:function(){
                                                roti.views['contentView'].items[0].title="Kartu Ujian";          
                                                roti.view('contentView',function(){
                                                siadinAPI.call('kartu_ujian',{},function(response){
                                                    if(response.status=="OK")
                                                    {
                                                        roti.add(roti.create({
                                                            type:'grid',
                                                            json:response.data
                                                        }),'htmlContent');

                                                    }
                                                });
                                                });
                                            }
                                        }                                                                                                     


                                },                                                                                            
                                {
                                        id:'keuangan',
                                        mode:'triple image',
                                        content:'',                                       
                                        bgcolor:'orangeDark',
                                        info:{
                                                name:'Info Keuangan',                                                
                                                badge:{
                                                    number:1,                                                    
                                                }
                                        },                                        
                                        event:{
                                            ontap:function(){
                                                roti.views['contentView'].items[0].title="Info Tagihan";          
                                                roti.view('contentView',function(){
                                                siadinAPI.call('info_tagihan',{},function(response){
                                                    if(response.status=="OK")
                                                    {
                                                        roti.add(roti.create({
                                                            type:'grid',
                                                            json:response.data
                                                        }),'htmlContent');

                                                    }
                                                });
                                                });

                                            }
                                        }                                                                                                     

                                },                                                           
                                {
                                        id:'setting',
                                        mode:'icon',
                                        content:'<i class="icon-cog"></i>',                                       
                                        bgcolor:'darken',
                                        info:{
                                                name:'Settings',                                                
                                        },                                        

                                },                                                                                        
                                {
                                        id:'store',
                                        mode:'icon',
                                        content:'<img src="images/Market128.png"/>',                                       
                                        bgcolor:'green',
                                        info:{
                                                name:'Dinus Tech',                                                
                                        },                                        

                                },       
                                {
                                       id:'chat',
                                        mode:'icon',
                                        content:'<i class="icon-comments-4"></i>',                                       
                                        bgcolor:'orange',
                                        info:{
                                                name:'Chat',                                                
                                        },                                        

                                },                                                                                                                                                   
                        ]
                }
        ]
});

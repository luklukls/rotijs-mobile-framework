roti.newView('home',{
        id:'home',
        items:[
                {
                        type:'tile',                        
                        id:'tile',
                        items:[
                                {
                                        id:'login',
                                        icon:true,
                                        content:'<img src="images/shield-user.png">',                                        
                                        bgcolor:'green',
                                        info:{
                                                name:'Login'                                                
                                        },
                                        tap:{                                            
                                            view:'login',
                                            title:'Login',
                                            data:{

                                            }
                                        }

                                },

                                {
                                        id:'beasiswa',
                                        icon:true,
                                        content:'<i class="icon-rocket"></i>',                                       
                                        bgcolor:'purple',
                                        info:{
                                                name:'Scholarship',
                                                badge:{
                                                    number:3,
                                                    bgcolor:'blue'
                                                }                                                
                                        },
                                        tap:{
                                            view:'contentView',
                                            title:'Scholarship',
                                            data:{
                                                call:'page',
                                                name:'beasiswa',

                                            }
                                        }

                                },                                


                                {
                                        id:'search',
                                        icon:true,
                                        content:'<i class="icon-search"></i>',                                        
                                        bgcolor:'red',
                                        info:{
                                                name:'Search'                                                
                                        },
                                        tap:{
                                            view:'search',
                                            title:'Search',    
                                            data:{
                                                call:'page',
                                                name:'search'
                                            }                                        
                                        }                                        
                                },                                

                                {
                                        id:'pasca',
                                        mode:'image',                                        
                                        content:'<img src="images/pps.jpg" class="place-right">',                                        
                                        bgcolor:'pinkDark',
                                        info:{
                                                name:'PascaSarjana'                                                
                                        },
                                        tap:{
                                            view:'contentView',
                                            title:'PascaSarjana',                                                                                        
                                            data:{
                                                call:'page',
                                                name:'pasca'
                                            }
                                        }                                        
                                },     

                                {
                                        id:'profile',
                                        mode:'double',
                                        content:'<img src="images/rektor.jpg" class="place-left" width="60px">'+
                                        '<h6 style="margin-bottom: 5px;">Dr.Ir. Edi Nursasongko M.Kom</h6>'+
                                        '<p >'+
                                            'Sambutan Rektor Universitas Dian Nuswantoro ...'+
                                        '</p>',                                        
                                        bgcolor:'blue',
                                        info:{
                                                name:'Dinus profile',
                                                badge:{
                                                        number:'8',
                                                        bgcolor:'red'
                                                }

                                        },
                                        tap:{
                                            view:'profile',
                                            title:'Dinus Profile' ,
                                            data:{
                                                call:'page',
                                                name:'profile'
                                            }
                                        },                                                                                                                                             


                                },                                

                                {
                                        id:'announcement',
                                        mode:'double',
                                        content:'<h5 id="announcement_title"></h5><p id="announcement_text"></p>',                                        
                                        bgcolor:'orangeDark',
                                        info:{
                                                icon:'Rss128.png',
                                                badge:{                                              
                                                        number:'10',
                                                        bgcolor:'green'
                                                }                                                
                                        },
                                        tap:{
                                            view:'announcement',
                                            title:'Info Akanemik',

                                        },
                                        event:{
                                            onready:function(){
                                                siadinAPI.call('info_akademik',{},function(response){                                                
                                                if(response.data)
                                                {                                                    
                                                    if(response.status=="OK")
                                                    {
                                                        
                                                            $('#announcement_title').html(response.data[0].judul);
                                                            $('#announcement_text').html(response.data[0].info.substring(0,50).replace(/(<\?[a-z]*(\s[^>]*)?\?(>|$)|<!\[[a-z]*\[|\]\]>|<!DOCTYPE[^>]*?(>|$)|<!--[\s\S]*?(-->|$)|<[a-z?!\/]([a-z0-9_:.])*(\s[^>]*)?(>|$))/gi, '')+'...');
                                                            $('#announcement .badge').html(response.data.length);
                                                        
                                                    }
                                                }
                                                });
                                            },
                                            ontap:function(){
                                                siadinAPI.call('info_akademik',{},function(response){
                                                    if(response.status=="OK")
                                                    {
                                                        roti.views['contentView'].items[0].title="Info Akademik";          
                                                        roti.views['contentView'].id="infoaka";       

                                                        roti.view('contentView');
                                                        var items=[];
                                                        for(var i in response.data)
                                                        {
                                                            var o = new Object();
                                                            o.title=response.data[i].judul+'<small> - '+response.data[i].wktKrm+'</small>';
                                                            o.text=response.data[i].info;
                                                            o.class="infoaka";
                                                            o.id=i;

                                                            if(o.text)
                                                            {
                                                                o.text=o.text.substring(0,100).replace(/(<\?[a-z]*(\s[^>]*)?\?(>|$)|<!\[[a-z]*\[|\]\]>|<!DOCTYPE[^>]*?(>|$)|<!--[\s\S]*?(-->|$)|<[a-z?!\/]([a-z0-9_:.])*(\s[^>]*)?(>|$))/gi, '')+'...';
                                                            }
                                                            items.push(o);
                                                        }
                                                        roti.add(roti.create({
                                                            type:'listview',
                                                            mode:'',                                                            
                                                            items:items,
                                                        }),'htmlContent');

                                                        $('.infoaka').tap(function(){
                                                            var n=$(this).attr('data-id');
                                                            siadinAPI.call('info_akademik',{},function(response){
                                                                if(response.status=="OK")
                                                                  {
                                                                     roti.views['contentsubView'].items[0].title=response.data[n].judul;          
                                                                     roti.view('contentsubView');
                                                                     $('#htmlsubContent').html('<strong>'+response.data[n].wktKrm+'</strong><br/>'+response.data[n].info);
                                                                     roti.menu='infoaka';
                                                                   }
                                                            });
                                                        });

                                                    }
                                                });
                                            }
                                        }                                                                                                     

                                },      
                                {
                                        id:'jadwal',
                                        icon:true,
                                        content:'<i class="icon-calendar"></i>',                                        
                                        bgcolor:'darken',
                                        info:{
                                                name:'Schedules'                                                
                                        },
                                        tap:{
                                            view:'contentView',
                                            title:'Kalender Akademik',                                                                                                                                                                                
                                            data:{

                                                call:'page',
                                                name:'jadwal'                                       
                                            }
                                        }

                                },                                                                   
                                {
                                        id:'relation',
                                        icon:true,
                                        content:'<i class="icon-link"></i>',                                        
                                        bgcolor:'greenLight',
                                        info:{
                                                name:'Relations'                                                
                                        },
                                        tap:{
                                            view:'contentView',
                                            title:'Kerjasama',                                                                                                                                                                                
                                            data:{
                                                call:'page',
                                                name:'relation'                                                
                                            }
                                        }

                                },                                
                                {
                                        id:'researches',
                                        icon:true,
                                        content:'<i class="icon-lab"></i>',                                        
                                        bgcolor:'greenDark',
                                        info:{
                                                name:'Researches'                                                
                                        },
                                        tap:{
                                            view:'contentView',
                                            title:'Researches',                                                                                                                                                                                
                                            data:{

                                                call:'page',
                                                name:'researches'                                     
                                            }
                                        }

                                },                                
                                {
                                        id:'paper',
                                        icon:true,
                                        content:'<i class="icon-book"></i>',                                        
                                        bgcolor:'orange',
                                        info:{
                                                name:'Paper & Jurnal',

                                        },
                                        tap:{
                                            view:'contentView',
                                            title:'Paper & Jurnal',                                                                                                                                                                                
                                            data:{

                                                call:'page',
                                                name:'paper'                                       
                                            }
                                        }

                                },                                
                                {
                                        id:'hospitality',
                                        icon:true,
                                        content:'<i class="icon-heart"></i>',                                        
                                        bgcolor:'red',
                                        info:{
                                                name:'Hospitality',

                                        },
                                        tap:{
                                            view:'contentView',
                                            title:'Hospitality',                                                                                                                                                                                
                                            data:{

                                                call:'page',
                                                name:'hospitality'                                      
                                            }
                                        }

                                },                                
                                {
                                        id:'career',
                                        icon:true,
                                        content:'<i class="icon-user-3"></i>',                                        
                                        bgcolor:'pinkDark',
                                        info:{
                                                name:'Career',

                                        },
                                        tap:{
                                            title:'Career',                                                                                                                                                                                
                                            view:'contentView',
                                            data:{

                                                call:'page',
                                                name:'career'                                      
                                            }
                                        }

                                },                                                                
                                {
                                        id:'entepreneurship',
                                        icon:true,
                                        content:'<i class="icon-accessibility"></i>',                                        
                                        bgcolor:'blueDark',
                                        info:{
                                                name:'Entepreneurship',

                                        },
                                        tap:{
                                            view:'contentView',
                                            title:'Entepreneurship',                                                                                                                                                                                
                                            data:{

                                                call:'page',
                                                name:'entepreneurship'
                                            }
                                        }

                                },                                                                


                        ]
                }
        ]
});

roti.newView('login',{
	bgcolor:'white',
	anim:'pop_in',	
	id:'login',
	items:[
		{
			type:'toolbar',
			title:'title'			
		},
		{
			type:'progressbar',
			id:'progressbar',
			value:'100'			

		},	
		{
			type:'text',
			label:'username',
			id:'username',
			value:$.jStorage.get('login')
		},
		{
			type:'password',
			label:'password',
			id:'password'
		},
		{
			type:'button',
			id:'loginbtn',
			label:'Login',
			class:'place-right',	
			style:'margin-right:0px',
			event:{
				ontap:function(){
					siadinAPI.call('login',{username:$('#username').val(),password:$('#password').val()},function(response){
						if(response.status=="OK")
						{
							
							roti.view('mhs');
							roti.menu='mhs';
							siadinAPI.call('inbox',{},function(response){
								if(response.status=="OK")
								{
									$('#inbox_title').html("Pesan Masuk");	
									$('#inbox_info').html(response.data[0].subject);
								}else
								{
									$('#inbox_title').html("Pesan Masuk");
									//$('#inbox_info').html("Tidak ada pesan baru");
								}
							});
							$('#profile_title').html(response.data[0].nama);
							$('#profile_info').html(response.data[0].nim+'<br/>'+response.data[0].alm_asal+'<br/>'+response.data[0].email_google);

							$('#inbox_info').html('From Michel, Panduang menggunakan Siadin Mobile');
							
						}else
						{
							roti.alert('Incorect Login','Pastikan login anda benar');
						}
					});
				}
			}
		}

	]
});
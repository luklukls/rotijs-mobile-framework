
var roti={
	init:function(start){
		siadinAPI.call('view',null,function(response){            
            var nn=response.data.length;
            var mm=0;
            
            for(var i in response.data)
            {                
                $.cachedScript('http://dinus.ac.id/androidAPI/'+response.data[i].url).done(function(script, textStatus) {                            		        
                    mm++;
                    if(mm>=nn)
                    {
                        start();                        
                    }
                }); 
            }
            
        });		
	},
	menuon:true,
	clear:function(){
		$('#app').html('');
	},	
	event:[],
	views:new Array(),
	addEvent:function(id,ev)
	{
		var o= new Object();
		o.e=ev;
		o.id=id;
		roti.event.push(o);


	},
	newView:function(objname,obj)
	{
		roti.views[objname]=obj;
	},
	getView:function(viewName)
	{
		return roti.views[viewName];
	},
	loadFile:function (filename, filetype){
		 if (filetype=="js"){ //if filename is a external JavaScript file
		  var fileref=document.createElement('script')
		  fileref.setAttribute("type","text/javascript")
		  fileref.setAttribute("src", filename)
		 }
		 else if (filetype=="css"){ //if filename is an external CSS file
		  var fileref=document.createElement("link")
		  fileref.setAttribute("rel", "stylesheet")
		  fileref.setAttribute("type", "text/css")
		  fileref.setAttribute("href", filename)
		 }
		 if (typeof fileref!="undefined")
		  document.getElementsByTagName("head")[0].appendChild(fileref)
	},
	giveVal:function(val,def,newval)
	{
		if(typeof val=="undefined")
							{
								return def;
							}else
							{
								return newval;
							}
	},
	view:function(viewName,callback){
		if(typeof callback!="function")
		{
			callback = function(){};
		}
		if($('.x'+viewName).length>0)
		{
			$('.view').hide();
			//console.log(viewName);
			$('.x'+viewName).show();
		}else
		{
		
				
			var target="app";

			var view=roti.views[viewName];
			view.id=roti.giveVal(view.id,'',view.id);
			view.anim=roti.giveVal(view.anim,'',view.anim);
			view.class=roti.giveVal(view.class,"",view.class);
			view.borderColor=roti.giveVal(view.borderColor,"",view.borderColor);
			view.bgcolor=roti.giveVal(view.bgcolor,"",view.bgcolor);
			var html='<div anim="'+view.anim+'" class="view x'+viewName+' bg-color-'+view.bgcolor+' '+view.class+'" id="'+view.id+'" >';
			for(var k in view.items)
			{
				html+=roti.create(view.items[k]);
			}	
			html+='</div>';
			if(viewName=="contentView")
			{
				roti.menuon=false;
			}
			if(view.anim!='')
			{
				$(html).appendTo('#'+target).addClass(view.anim).bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(){				

					$(this).removeClass(view.anim);					
			        $('#progressbar .bar').css('width','0%');
		            $('#progressbar .bar').animate({'width':'100%'},1000);		            
					callback();
					var t=$(this);
					$('.back-button,.icon-cancel').tap(function(){
						if(roti.menu=="infoaka")
						{
							roti.menu="home";
						}
						var anim=t.attr('anim');
						
						setTimeout(function(){$('#'+roti.menu).show();t.remove();},400);
						setTimeout(function(){roti.menuon=true;},4000);
						t.addClass(anim.replace('_in','_out'));

					});

				});					
			}else
			{
				var t=$(html).appendTo('#'+target);
			        $('#progressbar .bar').css('width','0%');
		            $('#progressbar .bar').animate({'width':'100%'},1000);

					callback();
					$('.back-button,.icon-cancel').tap(function(){				
						if(roti.menu=="infoaka")
						{
							roti.menu="home";
						}

					$('#'+roti.menu).show();	
						t.remove();
						
					});

				
			}


			for(var i in roti.event)	
			{

				
				{
					
					$('#'+roti.event[i].id).attr('data-id',i);
					$('#'+roti.event[i].id).tap(function(){
							
						roti.event[$(this).attr('data-id')].e.ontap();
						
					});
					if(roti.event[i].e.onready)
					{
						if(viewName=='home')
						{
							roti.event[i].e.onready();
						}
					}
					
				}

			}							

		
		}	
		//console.log(roti.event);

	},
	create:function(data)
	{
		var html='';
		if(data.event)
		{
			if(data.id)
			{
				
				roti.addEvent(data.id,data.event);
				
			}
		}
				switch(data.type)
				{
					case 'toolbar':
						html+='<div class="topnavbox"><a style="float:left" href="#" class="back-button page-back"></a><h3 >&nbsp;&nbsp;'+data.title+'</h3></div>';
					break;
					case 'toolbar2':
						html+='<div class="topnavbox"><h3 style="width:380px;">'+data.title+'</h3><a style="position:absolute;right:5px;top:10px;font-size:30px;" href="#"  class="icon-cancel"></a></div>';
					break;

					case 'tile' :
						data.class=roti.giveVal(data.class,"",data.class);
						html+='<div class="tiles clearfix '+data.class+'">';
						for(var n in data.items)
						{

							var ini=data.items[n];
							if(ini.event)
							{
								if(ini.id)
								{
									
									roti.addEvent(ini.id,ini.event);
									

								}
							}

							ini.slider=roti.giveVal(ini.slider,"tile-slider",ini.slider);
							ini.icon=roti.giveVal(ini.icon,"","icon");
							ini.info.name=roti.giveVal(ini.info.name,'','<span class="name">'+ini.info.name+'</span>');
							ini.info.text=roti.giveVal(ini.info.text,'','<p class="text">'+ini.info.text+'</p>');
							ini.info.icon=roti.giveVal(ini.info.icon,'','<img class="icon" src="images/'+ini.info.icon+'">');
							ini.id=roti.giveVal(ini.id,'',ini.id);
							ini.tap=roti.giveVal(ini.tap,'',ini.tap);
							if(ini.info.name)
							{
								ini.info.text='';
								ini.info.icon='';
							}
							html+='<div id="'+ini.id+'" sender="'+ini.bgcolor+'" class="tile '+ini.mode+' bg-color-'+ini.bgcolor+' '+ini.icon+'" data-role="'+ini.slider+'" tap='+"'"+JSON.stringify(ini.tap)+"'"+'>';
								html+='<div class="tile-content">'+ini.content+'</div>';
									html+='<div class="brand bg-color-'+ini.info.bgcolor+'">';
									html+=ini.info.name;
									html+=ini.info.text;
									html+=ini.info.icon;
									if(ini.info.badge)
									{
										html+='<div class="badge bg-color-'+ini.info.badge.bgcolor+'">'+ini.info.badge.number+'</div>';
									}
									html+='</div>';
							html+='</div>';
						}
						html+='</div>';
					break;
					case 'listview' :
					html+='<ul class="listview '+data.class+' '+data.mode+'">';
						
						for(var n in data.items)
						{						

							var ini=data.items[n];
							ini.id=roti.giveVal(ini.id,'',ini.id);							
							html+='<li data-id="'+ini.id+'" class="'+ini.class+'">';
								
								html+='<div class="data"><h4>'+ini.title+'</h4>';
								html+='<p>'+ini.text+'</p></div>';

							html+='</li>';
						}
					html+='</ul>';
					break;					
					case 'inbox' :
					html+='<ul class="replies">';

							var ini=data;
							if(typeof ini.bgcolor=="undefined")
							{
								ini.bgcolor="green";
							}
							ini.id=roti.giveVal(ini.id,'',ini.id);
							html+='<li id="'+ini.id+'" class="bg-color-'+ini.bgcolor+'">';
							if(typeof ini.icon =="undefined")
							{							
								ini.icon="michael.jpg";
							}
								html+='<div class="avatar"><img src="images/'+ini.icon+'"/></div>';
								html+='<div class="reply">';
				                	html+='<div class="date">'+ini.date+'</div>';
				                	html+='<div class="author">'+ini.title +'</div>';
				                	html+='<div class="text">'+ini.text+'</div>';
				            	html+='</div>';
							html+='</li>';
						
					html+='</ul>';
					break;	
					case 'alert' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.class=roti.giveVal(ini.class,'',ini.class);

						html+='<div class="notices '+ini.class+'" id="'+ini.id+'" >';
						html+='<div class="bg-color-'+ini.bgcolor+'">';
			            html+='<a href="#" class="close"></a>';
			            html+='<div class="notice-header">'+ini.title+'</div>';
			            html+='<div class="notice-text"> '+ini.text+'</div>';
			            html+='</div>';
						html+='</div>';
								
						

					break;	
					case 'grid' :
						var ini=data;
						
						ini.id=roti.giveVal(ini.id,'',ini.id);

						if(ini.json)
						{
							
						   html+='<table id="'+ini.id+'" class="bordered">';
						      html+='<thead>';

						      $.each( ini.json[0], function( key, value ) {
						          html+='<td>'+key+'</td>';


						      });
						      html+='</thead>';
						      html+='<tbody>';
						      for(var i in ini.json)
						      {
						        html+='<tr>';
						        $.each( ini.json[i], function( key, value ) {
						          html+='<td>'+value+'</td>';

						        });
						        html+='</tr>';
						     }
						     html+='</tbody>';
						    //console.log(html);					
						}		
						

					break;						
					case 'checkbox' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<label class="input-control checkbox"><input id="'+ini.id+'" type="checkbox" '+ini.disabled+'><span class="helper">'+ini.label+'</span></label>';						
					break;						
					case 'switch' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<label class="input-control switch"><input id="'+ini.id+'" type="checkbox" '+ini.disabled+'><span class="helper">'+ini.label+'</span></label>';						
					break;											
					case 'radio' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<label class="input-control radio"><input id="'+ini.id+'" type="checkbox" '+ini.disabled+'><span class="helper">'+ini.label+'</span></label>';						
					break;											
					case 'button' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.class=roti.giveVal(ini.class,'',ini.class);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.style=roti.giveVal(ini.label,'',ini.style);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<input id="'+ini.id+'" type="button" '+ini.disabled+' value="'+ini.label+'" class="'+ini.class+'" style="'+ini.style+'">';		
					break;											
					case 'submit' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<input id="'+ini.id+'" type="submit" '+ini.disabled+' value="'+ini.label+'">';						
					break;																
					case 'reset' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<input id="'+ini.id+'" type="reset" '+ini.disabled+' value="'+ini.label+'">';						
					break;																
					case 'text' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<div class="input-control text"><input id="'+ini.id+'" type="text" '+ini.disabled+' placeholder="'+ini.label+'" value="'+ini.value+'"><button class="helper"></button></div>';						
					break;											
					case 'password' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<div class="input-control password"><input id="'+ini.id+'" type="password" '+ini.disabled+' placeholder="'+ini.label+'"><button class="helper"></button></div>';						
					break;											
					case 'search' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<div class="input-control text"><input id="'+ini.id+'" type="text" '+ini.disabled+' placeholder="'+ini.label+'"><button class="btn-search"></button></div>';						
					break;											
					case 'textarea' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.label=roti.giveVal(ini.label,'',ini.label);
						ini.disabled=roti.giveVal(ini.disabled,'',ini.disabled);
						html+='<div class="input-control textarea"><textarea id="'+ini.id+'" type="text" '+ini.disabled+' placeholder="'+ini.label+'"></textarea></div>';						
					break;											
					case 'progressbar' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);
						ini.value=roti.giveVal(ini.value,'0',ini.value);
						ini.bgcolor=roti.giveVal(ini.bgcolor,'',ini.bgcolor);
						
						html+='<div class="progress-bar" id="'+ini.id+'"><div class="bar bg-color-'+ini.bgcolor+'" style="width: '+ini.value+'%"></div></div>';						
					break;		
					case 'div' :
						var ini=data;
						ini.id=roti.giveVal(ini.id,'',ini.id);

						
						html+='<div class="divbox" id="'+ini.id+'">'+ini.html+'</div>';						
					break;																

				}
		return html;
	},
	add:function(htmlobj,target)
	{
		
		if(arguments[2])
		{
			$('#'+target).append(htmlobj);	
		}else
		{
			$('#'+target).html(htmlobj);
		}
	},

	datalink:function(siadinAPI){
                var w=browser.getViewWdth();
                var h=browser.getViewHgt();

                var scalex=w/400;
                $('#app').css('-webkit-transform-origin','0 0');
                $('#app').css('-webkit-transform','translate(0px) scale('+scalex+')');		
	        $('a,div,p,span,img').each(function() {
	        	
	            this.addEventListener("touchstart", function(e) {
	            	console.log('touchstart OFF');
	                e.preventDefault();
	                
	            }, false);
	            this.addEventListener("touchend", function(e) {
	                e.preventDefault();                
	            }, false);
	        });

			$('.tile').tap(function(){			
			    if(roti.menuon)
			    {				
				var obj=JSON.parse($(this).attr('tap'));			
				if(obj.data)
				{
				
				roti.views[obj.view].items[0].title=obj.title;			
				roti.views[obj.view].items[1].bgcolor=$(this).attr('sender');							
				roti.view(obj.view,function(){
					
					if(obj.data.call=='page')
					{
						siadinAPI.call('page',{name:obj.data.name},function(response){
							if(response.status=="OK")
							{
								$('#htmlContent').html(response.data[0].html);
							}
						});
					}					

				});
				

				}
				}
			});				

	},
	alert:function(title,message){
			var err=roti.create({
					type:'alert',
					id:'alert',
					class:'place-center',
					title:'<span style="color:#fff">'+title+'</span>',
					text: '<span style="color:#fff;font-size:14px;">'+message+'</span>',
					bgcolor:'orangeDark'

			});
			roti.add(err,'app',true);		

			$('.close').tap(function(){
				$(this).parent().remove();
			});
	}

	
}
            var pushNotification;
            function onDeviceReady() {
                $("#app-status-ul").append('<li>deviceready event received</li>');
                
                pushNotification = window.plugins.pushNotification;
                if (device.platform == 'android' || device.platform == 'Android') {
                    pushNotification.register(successHandler, errorHandler, {"senderID":"591244244324","ecb":"onNotificationGCM"});
                } else {
                    pushNotification.register(tokenHandler, errorHandler, {"badge":"true","sound":"true","alert":"true","ecb":"onNotificationAPN"});
                }
                            }
                function onNotificationGCM(e) {
                                $("#app-status-ul").append('<li>EVENT -> RECEIVED:' + e.event + '</li>');
                                
                                switch( e.event )
                                {
                                    case 'registered':
                                    if ( e.regid.length > 0 )
                                    {
                                        $("#app-status-ul").append('<li>REGISTERED -> REGID:' + e.regid + "</li>");
                                        // Your GCM push server needs to know the regID before it can push to this device
                                        // here is where you might want to send it the regID for later use.
                                        console.log("regID = " + regID);
                                    }
                                    break;
                                    
                                    case 'message':
                                    $("#app-status-ul").append('<li>MESSAGE -> MSG: ' + e.message + '</li>');
                                    $("#app-status-ul").append('<li>MESSAGE -> MSGCNT: ' + e.msgcnt + '</li>');
                                    break;
                                    
                                    case 'error':
                                    $("#app-status-ul").append('<li>ERROR -> MSG:' + e.msg + '</li>');
                                    break;
                                    
                                    default:
                                    $("#app-status-ul").append('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
                                    break;
                                }
                            }
            
            function tokenHandler (result) {
                $("#app-status-ul").append('<li>token: '+ result +'</li>');
                // Your iOS push server needs to know the token before it can push to this device
                // here is where you might want to send it the token for later use.
            }

            function successHandler (result) {
                $("#app-status-ul").append('<li>success:'+ result +'</li>');
            }
            
            function errorHandler (error) {
                $("#app-status-ul").append('<li>error:'+ error +'</li>');
            }
            
            document.addEventListener('deviceready', onDeviceReady, true);
